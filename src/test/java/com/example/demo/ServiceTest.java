package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ServiceTest {

    @Autowired
    Service service = new Service();

    @Test
    void givenListShouldReturnList() {
        List<String> list = service.books();


        List<String> result = service.getAllBooks();

        assertEquals(list.size(),result.size());
    }

    @Test
    void getById() {
        List<String> list = service.books();
        int id = 1;
        String mock  = list.get(id);

        String result = service.getById(id);

        assertEquals(mock,result);

    }
}