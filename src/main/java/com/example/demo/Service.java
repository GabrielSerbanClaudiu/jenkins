package com.example.demo;

import org.springframework.stereotype.Controller;

import java.util.Arrays;
import java.util.List;

@Controller
public class Service {

    public List<String> books(){
        List<String> books = Arrays.asList("carte1","carte2","carte3");
        return books;
    }


    public List<String> getAllBooks(){
      return  books();
    }

    public String getById(Integer id){
        List<String> books = books();

        return books.get(id);
    }

}
