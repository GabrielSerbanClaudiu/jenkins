package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class demoController {


    @Autowired
    Service service;


    @GetMapping("/books")
    public List<String> getBooks(){
        return service.getAllBooks();
    }

    @GetMapping("/{id}")
    public String getBooks(@PathVariable Integer id){
        return service.getById(id);
    }

}
